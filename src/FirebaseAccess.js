import 'firebase';
import ENV from './utils/environment';

// http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

const generateTradeoff = (displayName, tradeoffId) => ({
  history: [],
  summary: {
    balance: 0,
    dropdownData: [
      {
        "children": [
          {
            "delta": -10,
            "desc": "First Drink",
            "label": "First (-10)"
          },
          {
            "delta": -20,
            "desc": "Subsequent Drink",
            "label": "Subsequent (-20)"
          }
        ],
        "name": "Drinks"
      },
      {
        "children": [
          {
            "delta": 10,
            "desc": "10 pushups",
            "label": "Ten (10)"
          },
          {
            "delta": 20,
            "desc": "20 pushups",
            "label": "Twenty (20)"
          }
        ],
        "name": "Pushups"
      },
      {
        "name": "Custom..."
      }
    ],
    name: displayName,
    tradeoffId: tradeoffId,
    units: 'pushups' // TODO - unnecesary?
  }
});

class FirebaseAccess {
  constructor() {
    this.firebaseApp_ = null;
    this.db_ = null;
  }

  ensureInit_() {
    if (this.firebaseApp_) {
      return;
    }
    const firebasePrefix = ENV.FIREBASE_PREFIX;

    this.firebaseApp_ = firebase.initializeApp({
      apiKey: ENV.FIREBASE_apiKey,
      authDomain: ENV.FIREBASE_authDomain,
      databaseURL: ENV.FIREBASE_dbUrl,
      storageBucket: "",
    });
    this.db_ = this.firebaseApp_.database();
    this.authProvider_ = new firebase.auth.GoogleAuthProvider();
  }

  /**
   * @param {string} tradeoffId
   * @returns {Promise}
   * @resolves {id, balance, dropdown}
   */
  getTradeoff(tradeoffId) {
    this.ensureInit_();

    // this.migrate(tradeoffId);

    return this.db_.ref(`${tradeoffId}/summary`)
    .once('value')
    .then(snapshot => {
      return snapshot.val();
    });
  }

  // TODO - probably eventually want some paginated version of this since right
  // now returns 100 results
  /**
   * @returns {{date: string, delta: number, desc: string}[]}
   */
  getAllHistoryItems(tradeoffId) {
    this.ensureInit_();

    return this.db_.ref(`${tradeoffId}/history`)
    .limitToLast(100)
    .once('value')
    .then(snapshot => {
      const val = snapshot.val();
      return Object.values(val).reverse();
    });
  }

  /**
   * Creates a new history item object and sends it to the server, using two
   * phase commits.
   * @returns {{tradeoffId: string, date: string?}
   */
  addHistoryItem({tradeoffId, delta, date, desc}) {
    this.ensureInit_();

    return this.db_.ref(`${tradeoffId}/history`)
    .push({
      date: date.toISOString(),
      delta,
      desc
    })
    .then(() => {
      return this.db_.ref(`${tradeoffId}/summary/balance`)
      .transaction(current => current + delta)
    })
    .then(() => ({ date, tradeoffId }));
  }

  /**
   *
   */
  uploadDropdownData(tradeoffId, data) {
    this.ensureInit_();

    return this.db_.ref(`${tradeoffId}/summary/dropdownData`)
    .set(data);
  }

  getSummary() {
    this.ensureInit_();
    return this.db_.ref('summary')
    .once('value')
    .then(snapshot => {
      return snapshot.val();
    });
  }

  createTradeoff(displayName, uid) {
    this.ensureInit_();

    const tradeoffId = guid();
    return this.db_.ref(`tradeoffIds/${uid}`)
    .set(tradeoffId)
    .then(() => {
      return this.db_.ref(tradeoffId)
      .set(generateTradeoff(displayName, tradeoffId));
    })
  }

  signIn(email, password) {
    this.ensureInit_();

    return this.firebaseApp_.auth().signInWithEmailAndPassword(email, password)
    .then(user => {
      return {
        uid: user.uid,
        displayName: user.displayName,
      };
    })
  }

  signUp(email, password, displayName) {
    this.ensureInit_();

    let newUser;
    return this.firebaseApp_.auth().createUserWithEmailAndPassword(email, password)
    .then(user => {
      newUser = user;
      return user.updateProfile({displayName})
    })
    .then(() => {
      // updateProfile returns nothing, but could result in failure. If it didn't
      // fail, return the user we got from createUser.
      return {
        uid: newUser.uid,
        displayName
      };
    });
  }
}


export default FirebaseAccess;

// TODO - probably a sign that we dont need to be a class?
let firebaseAccess;
export const getFirebaseSingleton = () => {
  if (!firebaseAccess) {
    firebaseAccess = new FirebaseAccess();
  }
  return firebaseAccess;
};
