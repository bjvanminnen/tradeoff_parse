const AUTH_BEGIN = 'auth/AUTH_BEGIN';
const AUTH_FAILURE = 'auth/AUTH_FAILURE';
const AUTH_SUCCESS = 'auth/AUTH_SUCCESS';

const initialState = {
  failureMessage: undefined,
  userName: null,
  uid: null,
  waiting: false
};

export default function reducer(state = initialState, action) {
  if (action.type === AUTH_BEGIN) {
    return Object.assign({}, state, {
      waiting: true,
      failureMessage: null,
    });
  }

  if (action.type === AUTH_SUCCESS) {
    const { userName, uid } = action;
    return Object.assign({}, state, {
      failureMessage: null,
      waiting: false,
      userName,
      uid
    });
  }

  if (action.type === AUTH_FAILURE) {
    return Object.assign({}, state, {
      failureMessage: action.message,
      waiting: false
    });
  }

  return state;
}

export const authBegin = () => ({ type: AUTH_BEGIN });

export const authSuccess = (userName, uid) => ({
  type: AUTH_SUCCESS,
  userName,
  uid
});

export const authFailure = message => ({
  type: AUTH_FAILURE,
  message
});
