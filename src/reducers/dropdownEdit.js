import { ACTIVATE_TRADEOFF_ID } from '../reducers/activeTradeoff';
import { getFirebaseSingleton } from '../FirebaseAccess';

export const UPDATE_DROPDOWN_BEGIN = 'dropdownEdit/UPDATE_DROPDOWN_BEGIN';
export const UPDATE_DROPDOWN_SUCCESS = 'dropdownEdit/UPDATE_DROPDOWN_SUCCESS';
export const UPDATE_DROPDOWN_FAILURE = 'dropdownEdit/UPDATE_DROPDOWN_FAILURE';

export default function dropdownEditReducer(state = {}, action) {
  switch(action.type) {
  case ACTIVATE_TRADEOFF_ID:
    if (state[action.body]) {
      return state;
    }
    // create initial state for this tradeoffId
    return Object.assign({}, state, {
      [action.body]: {
        modifying: false,
        failed: false
      }
    });
  case UPDATE_DROPDOWN_BEGIN:
    return Object.assign({}, state, {
      [action.body.tradeoffId]: {
        modifying: true,
        failed: false
      }
    });
  case UPDATE_DROPDOWN_SUCCESS:
    return Object.assign({}, state, {
      [action.body.tradeoffId]: {
        modifying: false,
        failed: false
      }
  });
  case UPDATE_DROPDOWN_FAILURE:
    return Object.assign({}, state, {
      [action.body.tradeoffId]: {
        modifying: false,
        failed: true
      }
    });

  default:
    return state;
  }
}

// TODO - elsewhere we pass an obj, i.e. {tradeoffId, data}
export function updateDropdownData(tradeoffId, data) {
  return dispatch => {
    dispatch({
      type: UPDATE_DROPDOWN_BEGIN,
      body: {tradeoffId},
    });

    getFirebaseSingleton().uploadDropdownData(tradeoffId, data)
    .then(result => {
      dispatch({
        type: UPDATE_DROPDOWN_SUCCESS,
        body: {
          tradeoffId,
          newDropdownData: data
        }
      });
    })
    .catch(err => {
      dispatch({
        type: UPDATE_DROPDOWN_FAILURE,
        body: { tradeoffId },
        error: err
      });
    });
  }
}
