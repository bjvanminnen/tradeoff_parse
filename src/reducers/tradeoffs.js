import Immutable from 'immutable';
import {
  LOAD_TRADEOFF_SUMMARY,
  LOAD_FROM_SERVER_SUCCESS,
  LOAD_FROM_SERVER_FAILURE
} from '../TradeoffActions';
import { UPDATE_DROPDOWN_SUCCESS } from './dropdownEdit';


const HistoryItem = Immutable.Record({ // eslint-disable-line new-cap
  date: undefined,
  delta: undefined,
  desc: undefined,
  state: undefined,
});

export default function tradeoffsReducer(state = {}, action) {
  const type = action.type;

  if (type === LOAD_TRADEOFF_SUMMARY) {
    const summary = action.body;
    const uids = Object.keys(summary);
    return Object.assign({}, state, uids.reduce((obj, uid) => {
      const info = summary[uid];
      // Initialize any new ids
      // TODO - should init logic live elsewhere?
      if (!state[info.id]) {
        obj[info.id] =  {
          balance: 0,
          name: info.name,
          owner: uid,
          tradeoffId: info.id,
          // TODO - more descriptive names
          isWaiting: true,
          failed: false,
          history: Immutable.List() // eslint-disable-line new-cap
        };
      }
      return obj;
    }, {}));
  }

  // all other actions operate on a particular tradeoff
  let tradeoffId = action.body && action.body.tradeoffId;
  if (!tradeoffId) {
    return state;
  }

  return Object.assign({}, state, {
    [tradeoffId]: singleTradeoffReducer(state[tradeoffId], action)
  });
}

export function singleTradeoffReducer(tradeoffState, action) {
  switch(action.type) {
  case LOAD_FROM_SERVER_SUCCESS: {
    const { tradeoffId, balance, dropdownData, name } = action.body;
    return {
      ...tradeoffState,
      isWaiting: false,
      name,
      tradeoffId,
      balance,
      dropdownData,
    };
  }
  case LOAD_FROM_SERVER_FAILURE:
    return {
      ...tradeoffState,
      isWaiting: false,
      failed: true
    };

  case 'addHistory_begin':
    if (!tradeoffState) {
      throw new Error('addHistory_begin expects existing tradeoffState');
    }
    return Object.assign({}, tradeoffState, {
      balance: tradeoffState.balance + action.body.delta,
      history: tradeoffState.history.unshift(new HistoryItem({
        date: action.body.date,
        delta: action.body.delta,
        desc: action.body.desc,
        state: 'pending'
      }))
    });

  case 'addHistory_success':
    const target = new Date(action.body.date).getTime();

    // get appropriate item from list
    const history = tradeoffState.history;
    let historyItemKey;
    for (let i = history.size - 1; i >= 0 && !historyItemKey; i--) {
      if (history.get(i).get('date').getTime() === target) {
        historyItemKey = i;
      }
    }

    if (historyItemKey === undefined) {
      throw new Error('no historyItem found')
    }

    // update to non-pending
    return Object.assign({}, tradeoffState, {
      history: history.set(historyItemKey,
        history.get(historyItemKey).set('state', 'completed'))
    });

  case 'addHistory_failure':
    throw action.error;

  case 'getAllHistoryItems_success':
    return Object.assign({}, tradeoffState, {
      tradeoffId: action.body.tradeoffId,
      history: Immutable.List(action.body.results.map(item => { // eslint-disable-line new-cap
        return new HistoryItem({
          date: new Date(item.date),
          delta: item.delta,
          desc: item.desc,
          state: 'completed'
        });
      }))
    });

  case 'getAllHistoryItems_failure':
    throw action.error;

  case UPDATE_DROPDOWN_SUCCESS:
    return Object.assign({}, tradeoffState, {
      dropdownData: action.body.newDropdownData
    });

  default:
    return tradeoffState;
  }
}
