import { combineReducers } from 'redux';

import dropdownEditReducer from './dropdownEdit';
import activeTradeoffReducer from './activeTradeoff';
import tradeoffsReducer from './tradeoffs';
import authReducer from './auth';

export default combineReducers({
  auth: authReducer,
  activeTradeoff: activeTradeoffReducer,
  tradeoffs: tradeoffsReducer,
  dropdownEdit: dropdownEditReducer
});
