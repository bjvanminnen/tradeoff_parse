export const ACTIVATE_TRADEOFF_ID = 'activeTradeoff/ACTIVATE_TRADEOFF_ID';

export default function activeTradeoffReducer(state = null, action) {
  switch(action.type) {
  case ACTIVATE_TRADEOFF_ID:
    return action.body;
  default:
    return state;
  }
}

export function activateTradeoffId(tradeoffId) {
  return {
    type: ACTIVATE_TRADEOFF_ID,
    body: tradeoffId
  };
}
