import '../css/styles.css';
import '../css/codemirror.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import RouterHome from './components/RouterHome';

// import Logger from './Logger';
import tradeoffListReducer from './reducers/tradeoffList';
import { authSuccess } from './reducers/auth';

import ENV from './utils/environment';

window.__VERSION = VERSION;

const logger = createLogger({
  collapsed: true,
  predicate: () => !!ENV.LOG
});

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware, logger)(createStore);
const reduxStore = createStoreWithMiddleware(tradeoffListReducer);

reduxStore.subscribe(() => {
  const { uid, userName } = reduxStore.getState().auth;
  if (uid) {
    localStorage.setItem('uid', uid);
    localStorage.setItem('userName', userName);
  }
});

const uid = localStorage.getItem('uid');
const userName = localStorage.getItem('userName');
if (uid && userName) {
  reduxStore.dispatch(authSuccess(userName, uid));
}

ReactDOM.render(
  <div>
    <Provider store={reduxStore}>
      <RouterHome/>
    </Provider>
  </div>,
  document.getElementById('app')
);
