import 'firebase';
import ENV from '../utils/environment';

export default function run() {
  const firebaseApp_ = firebase.initializeApp({
    apiKey: ENV.FIREBASE_apiKey,
    authDomain: ENV.FIREBASE_authDomain,
    databaseURL: ENV.FIREBASE_dbUrl,
    storageBucket: "",
  }, 'addSummaryMigration');

  const db = firebaseApp_.database();

  db.ref('tradeoffIds')
  .once('value')
  .then(snapshot => {
    return snapshot.val();
  })
  .then(tradeoffIds => {
    return getSummary(db, tradeoffIds);
  })
  .then(summary => {
    return db.ref('summary')
    .set(summary);
  })
  .then(() => console.log('done'))
  .catch(err => console.error(err));
}

function getSummary(db, tradeoffIds) {
  const uids = Object.keys(tradeoffIds);
  const summary = {};
  return new Promise((resolve, reject) => {
    uids.forEach(uid => {
      const id = tradeoffIds[uid];
      db.ref(`${id}/summary/name`)
      .once('value')
      .then(snapshot => snapshot.val())
      .then(name => {
        summary[uid] = { name, id };
        if (Object.keys(summary).length === uids.length) {
          console.log(summary);
          resolve(summary);
        }
      });
    });
  });
}
