let envKeys = {
  staging: {
    LOG: true,
    FIREBASE_apiKey: "AIzaSyBGHdksG1wvrjL44IbEAIC4sKf7uCjVZIQ",
    FIREBASE_authDomain: "tradeoff-staging.firebaseapp.com",
    FIREBASE_dbUrl: "https://tradeoff-staging.firebaseio.com",
  },
  production: {
    FIREBASE_apiKey: "AIzaSyCfV9qQFDpBK8mWhwmeRWXhHejdTGznnlM",
    FIREBASE_authDomain: "tradeoff.firebaseapp.com",
    FIREBASE_dbUrl: "https://tradeoff.firebaseio.com",
  }
};
envKeys.development = envKeys.staging;

const ENV = envKeys[process.env.NODE_ENV];
export default ENV;
