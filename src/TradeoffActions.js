import { getFirebaseSingleton } from './FirebaseAccess';
import { authBegin, authFailure, authSuccess } from './reducers/auth';
import { activateTradeoffId } from './reducers/activeTradeoff';

export const LOAD_TRADEOFF_SUMMARY = 'tradeoffs/LOAD_TRADEOFF_SUMMARY';
export const LOAD_FROM_SERVER_SUCCESS = 'tradeoffs/LOAD_FROM_SERVER_SUCCESS';
export const LOAD_FROM_SERVER_FAILURE = 'tradeoffs/LOAD_FROM_SERVER_FAILURE';

export const loadTradeoffSummary = summary => ({
  type: LOAD_TRADEOFF_SUMMARY,
  body: summary
});

let store;

/**
Page load flow:
(S) Check local storage for existing ids
  (AS) If no ids load tradeoffs from server
(S) Activate a tradeoff (first of ids we got from server, or last active from localStorage)
(AS) Get all history items from server.
  - Could make this only hit the server if we dont already have items
  - Need to either make this conditional, or deal with merging/replacing existing items

Switch tradeoff flow
(S) Activate a tradeoff (given id)
(AS) Get all history items from server

Action primitives
loadTradoffIds - get a set of tradeoff ids either from local storage, or from the server
loadFailure - failure when trying to get our initial set of ids from the server
activateTradeoff - we've selected a tradeoff. we may or may not have history for it
loadedHistoryItems - got a bunch of history items from the server.
loadedHistoryItemsFailure - tried to load history items, but failed
addHistoryItemClient - client added a history item
addHistoryItemSuccess- server persisted added item
addHistoryItemFailure - failed to update server
updateDropdownDataClient
updateDropdownDataSuccess
updateDropdownDataFailure

Action creators
load
  - loadTradeoffsIds, loadFailure, activateTradeoff, loadedHistoryItems*
switch
  - activateTradeoff, loadedHistoryItems*
addHistoryItem
  - addHistoryItem*
updateDropdownData
  - updateDropdownData*

*/

export function loadCurrentUserTradeoff() {
  return (dispatch, getState) => {
    const state = getState();
    const { uid } = state.auth;

    if (!uid) {
      throw new Error('no current user');
    }

    getFirebaseSingleton().getSummary()
    .then(summary => {
      const myTradeoff = summary[uid];
      if (!myTradeoff) {
        throw new Error('no tradeoff for user');
      }

      dispatch(loadTradeoffSummary(summary));
      dispatch(activateTradeoffId(myTradeoff.id));
    });

  };
}

export function loadFromServer(tradeoffId) {
  // TODO - make smarter, in that we dont hit the server if we already have
  // in store?
  return dispatch => {
    getFirebaseSingleton().getTradeoff(tradeoffId)
    .then(result => {
      const { tradeoffId, balance, dropdownData, name } = result;
      dispatch({
        type: LOAD_FROM_SERVER_SUCCESS,
        body: {
          name,
          tradeoffId,
          balance,
          dropdownData
        }
      });
    })
    .catch(err => {
      // TODO - should wrap console.log in some sort of logging class that can
      // be turned on or off
      console.log(err);
      dispatch({
        type: LOAD_FROM_SERVER_FAILURE,
        body: { tradeoffId },
        error: err
      });
    });
  }
}

export function addHistory({tradeoffId, delta, desc, date}) {
  return dispatch => {
    dispatch({
      type: 'addHistory_begin',
      body: {tradeoffId, delta, desc, date},
    });

    getFirebaseSingleton().addHistoryItem({tradeoffId, delta, desc, date})
    .then(result => {
      dispatch({
        type: 'addHistory_success',
        body: result
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: 'addHistory_failure',
        body: { tradeoffId },
        error: err
      });
    });
  }
}

/**
 * @param {string}
 */
export function getAllHistoryItems(tradeoffId) {
  return dispatch => {
    // no 'begin' action, since unneeded
    getFirebaseSingleton().getAllHistoryItems(tradeoffId)
    .then(results => {
      dispatch({
        type: 'getAllHistoryItems_success',
        body: { tradeoffId, results },
      });
    })
    .catch(err => {
      console.log('getAllHistoryItems failure: ' + (err &&  err.message));
      // TODO - unit test this case
      dispatch({
        type: 'getAllHistoryItems_failure',
        body: tradeoffId,
        error: err
      });
    });
  }
}

export function signIn(email, password) {
  return dispatch => {
    dispatch(authBegin());
    getFirebaseSingleton().signIn(email, password)
    .then(({uid, displayName}) => {
      dispatch(authSuccess(displayName, uid));
    })
    .catch(err => {
      console.error(err);
      dispatch(authFailure(err.message));
    });
  };
};

export function signUp(email, password, displayName) {
  return dispatch => {
    dispatch(authBegin());

    getFirebaseSingleton().signUp(email, password, displayName)
    .then(({uid, displayName}) => {
      dispatch(authSuccess(displayName, uid));
      return getFirebaseSingleton().createTradeoff(displayName, uid);
    })
    .then(() => {
      console.log('created');
    })
    .catch(err => {
      console.error(err);
      dispatch(authFailure(err.message));
    });

  };
};
