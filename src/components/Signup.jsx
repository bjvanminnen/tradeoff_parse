import React from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { doAuth, signIn, signUp } from '../TradeoffActions';
import ButtonLink from './ButtonLink';

const styles = {
  col1: {
    textAlign: 'right',
    paddingRight: 5
  },
  input: {
    paddingTop: 2,
    paddingBottom: 2
  },
  authWait: {
    fontSize: 18,
    margin: 10
  }
};

class Signup extends React.Component {
  constructor(props) {
    super(props);

    this.signUp = this.signUp.bind(this);
  }

  componentDidUpdate() {
    if (this.props.uid) {
      this.props.router.push('/main')
    }
  }

  signUp() {
    if (this.refs.password.value !== this.refs.password2.value) {
      alert('passwords dont match');
      return;
    }

    this.props.signUp(this.refs.email.value, this.refs.password.value,
      this.refs.displayName.value);
  }

  render() {
    // TODO - logout if we come logged in?
    return (
      <div>
        <div>
          Note things are very hacked together at the moment. Please don't make
          an effort to break them.
        </div>
        <div>
          <b>Display name</b> is used to differentiate you from other users.
        </div>
        <div>
          <b>Email</b> is used for potential password resets in the future
        </div>
        <h1>Create Account</h1>
        <table style={{marginTop: 30}}>
          <tbody>
            <tr>
              <td style={styles.col1}>Display name:</td>
              <td style={styles.input}><input ref='displayName'/></td>
            </tr>
            <tr>
              <td style={styles.col1}>Email:</td>
              <td style={styles.input}><input ref='email' type="email"/></td>
            </tr>
            <tr>
              <td style={styles.col1}>Password:</td>
              <td style={styles.input}><input ref='password' type="password"/></td>
            </tr>
            <tr>
              <td style={styles.col1}>Confirm Password:</td>
              <td style={styles.input}><input ref='password2' type="password"/></td>
            </tr>
            <tr>
              <td colSpan="2" style={{textAlign: 'right'}}>
                <button onClick={this.signUp}>Sign up</button>
              </td>
            </tr>
          </tbody>
        </table>
        {this.props.authWaiting &&
          <div style={styles.authWait}>
            Authorizing...
          </div>
        }
        {this.props.authFailure &&
          <div style={{color: 'red'}}>
            {this.props.authFailure}
          </div>
        }
        <div>
          Have an account?
          <div>
            <ButtonLink to='/signin'>Sign in</ButtonLink>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(state => ({
  uid: state.auth.uid,
  authFailure: state.auth.failureMessage,
  authWaiting: state.auth.waiting
}), dispatch => ({
  doAuth() {
    dispatch(doAuth());
  },
  signUp(email, password, displayName) {
    dispatch(signUp(email, password, displayName));
  }
}))(withRouter(Signup));
