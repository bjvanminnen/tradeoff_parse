import React from 'react';
import moment from 'moment';

const DATE_FORMAT = 'MM/DD h:mma';

export default class HistoryRow extends React.Component {
  static propTypes = {
    delta: React.PropTypes.number.isRequired,
    date: React.PropTypes.instanceOf(Date).isRequired,
    desc: React.PropTypes.string.isRequired,
    state: React.PropTypes.oneOf(['pending', 'completed']).isRequired
  }

  render() {
    var style;
    if (this.props.state === 'pending') {
      style = { opacity: 0.5 };
    }
    return (
      <tr style={style}>
        <td>{this.props.delta}</td>
        <td>{this.props.desc}</td>
        <td>{moment(this.props.date).format(DATE_FORMAT)}</td>
      </tr>
    );
  }
}
