import React from 'react';
import {Table} from 'react-bootstrap';

class Paginator extends React.Component {
  setPage (page) {
    if (page < 1 || page > this.props.numPages) {
      return;
    }
    this.props.onSetPage(page);
  }

  createNumberedPage(number, active) {
    let className = 'page-number';
    if (active) {
      className += ' active disabled';
    }
    return (
      <li
          key={number}
          className={className}
          onClick={this.setPage.bind(this, number)}>
        <a href="javascript:void(0)">{number}</a>
      </li>
    );
  }

  render() {
    let numbered = [];
    let currentPage = this.props.page;
    let numPages = this.props.numPages;

    if (currentPage > 1) {
      numbered.push(this.createNumberedPage(currentPage - 1, false));
    }
    numbered.push(this.createNumberedPage(currentPage, true));
    if (currentPage < numPages) {
      numbered.push(this.createNumberedPage(currentPage + 1, false));
    }

    if (currentPage === 1 && currentPage + 2 <= numPages) {
      numbered.push(this.createNumberedPage(currentPage + 2, false));
    }
    if (currentPage === numPages && currentPage - 2 >= 1) {
      numbered = [this.createNumberedPage(currentPage - 2, false)]
        .concat(numbered);
    }

    return (
      <ul className='pagination' style={{marginTop: 0}}>
        <li
            className='page-first'
            onClick={this.setPage.bind(this, 1)}>
          <a href="javascript:void(0)">&lt;&lt;</a>
        </li>
        <li
            className='page-pre'
            onClick={this.setPage.bind(this, this.props.page - 1)}>
          <a href="javascript:void(0)">&lt;</a>
        </li>
        {numbered}
        <li
            className='page-next'
            onClick={this.setPage.bind(this, this.props.page + 1)}>
          <a href="javascript:void(0)">&gt;</a>
        </li>
        <li
            className='page-last'
            onClick={this.setPage.bind(this, this.props.numPages)}>
          <a href="javascript:void(0)">&gt;&gt;</a>
        </li>
      </ul>
    );
  }
}
Paginator.propTypes = {
  page: React.PropTypes.number.isRequired,
  numPages: React.PropTypes.number.isRequired,
  onSetPage: React.PropTypes.func.isRequired
};

export default class PaginatedTable extends React.Component {
  constructor() {
    super();
    this.state = {
      page: 1
    };
  }

  setPage(page) {
    this.setState({
      page: page
    });
  }

  render() {
    let rows = [];
    let numPages = Math.ceil(this.props.size / this.props.pageSize);
    let firstIndex = (this.state.page - 1) * this.props.pageSize;
    for (var i = 0; i < this.props.pageSize && this.props.size; i++) {
      rows.push(this.props.rowGenerator(firstIndex + i));
    }

    let style=Object.assign({}, { marginBottom: 0 }, this.props.style);
    return (
      <div>
        <Table striped bordered condensed hover style={style}>
          {this.props.children}
          <tbody>
            {rows}
          </tbody>
        </Table>
        <Paginator
          page={this.state.page}
          numPages={numPages}
          onSetPage={this.setPage.bind(this)}/>
      </div>
    );
  }
}

PaginatedTable.propTypes = {
  size: React.PropTypes.number.isRequired,
  pageSize: React.PropTypes.number.isRequired,
  rowGenerator: React.PropTypes.func.isRequired,
  style: React.PropTypes.object
};
