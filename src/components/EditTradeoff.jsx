import React from 'react';
import { connect } from 'react-redux';
import Codemirror from 'react-codemirror';
import HamburgerPanel from './HamburgerPanel';
import { loadFromServer } from '../TradeoffActions';
import { activateTradeoffId } from '../reducers/activeTradeoff';
import { updateDropdownData } from '../reducers/dropdownEdit';
import ENV from '../utils/environment';
import { Link } from 'react-router'

function generateCode(obj) {
  return JSON.stringify(obj, null, '  ');
}

// TODO - going to need ability to switch tradeoffs, like we have for main
// right now you need to first go to /main

export default connect(state => {
  const activeTradeoff = state.tradeoffs[state.activeTradeoff];
  const activeDropdownEdit = state.dropdownEdit[state.activeTradeoff];
  const tradeoffSummary = Object.keys(state.tradeoffs).map(uid => ({
    name: state.tradeoffs[uid].name,
    id: state.tradeoffs[uid].tradeoffId,
  }));
  return {
    isOwner: activeTradeoff.owner === state.auth.uid,
    name: activeTradeoff.name,
    tradeoffSummary,
    key: state.activeTradeoff,
    id: state.activeTradeoff,
    isWaiting: activeTradeoff.isWaiting,
    failed: activeTradeoff.failed,
    balance: activeTradeoff.balance,
    dropdownOptions: activeTradeoff.dropdownData,
    dropdownData: activeTradeoff.dropdownData,
    modifyingDropdown: activeDropdownEdit.modifying,
    modifyingDropdownFailed: activeDropdownEdit.failed
  };
})
(class extends React.Component {
  static propTypes = {
    isOwner: React.PropTypes.bool.isRequired,
    id: React.PropTypes.string.isRequired,
    tradeoffIds: React.PropTypes.arrayOf(React.PropTypes.string),
    isWaiting: React.PropTypes.bool.isRequired,
    failed: React.PropTypes.bool.isRequired,
    // TODO - required after loading from server
    dropdownData: React.PropTypes.array,
    modifyingDropdown: React.PropTypes.bool.isRequired,
    modifyingDropdownFailed: React.PropTypes.bool.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      code: '',
      readOnly: false
    };
  }

  componentWillMount() {
    this.props.dispatch(loadFromServer(this.props.id));
  }

  updateCode(newCode) {
    if (this.state.modifyingDropdown) {
      this.setState({code: this.state.code});
    } else {
      this.setState({code: newCode});
    }
  }

  resetCode() {
    this.setState({code: generateCode(this.props.dropdownData)});
  }

  submitCode() {
    if (this.props.modifyingDropdown) {
      return;
    }
    // TODO - validate code
    this.props.dispatch(
      updateDropdownData(this.props.id, JSON.parse(this.state.code)));
  }

  componentWillReceiveProps(newProps) {
    if (newProps.dropdownData) {
      this.setState({code: generateCode(newProps.dropdownData)});
    }
  }

  // TODO - could/shoudl share with Tradeoff
  onSwitchTradeoff(newId) {
    this.props.dispatch(activateTradeoffId(newId));
  }

  render() {
    if (this.props.isWaiting) {
      return (<div>Loading...</div>);
    }

    if (this.props.failed) {
      return (<div>Failed</div>);
    }

    // TODO - could be nice to have linting/better theme/smaller text
    const options = {
      lineNumbers: true,
      mode: 'JavaScript',
      json: true,
      tabSize: 2
    };

    // very hacky
    let element = document.getElementsByClassName('CodeMirror')[0];
    if (element) {
      let color = null
      if (this.props.modifyingDropdownFailed) {
        color = 'red';
      } else if (this.props.modifyingDropdown) {
        color = 'lightgray';
      }
      element.style.background = color;
    }

    const headerText = 'Edit: ' + this.props.name;

    return (
      <div>
        <HamburgerPanel
            readonly={!this.props.isOwner}
            headerText={headerText}
            tradeoffSummary={this.props.tradeoffSummary}
            switchTradeoff={this.onSwitchTradeoff.bind(this)}>
          <h4>{this.props.balance}</h4>
        </HamburgerPanel>
        <Codemirror
          value={this.state.code}
          onChange={::this.updateCode}
          options={options}/>
        {this.props.isOwner &&
          <div>
            <button onClick={::this.resetCode}>Reset</button>
            <button onClick={::this.submitCode}>Submit</button>
          </div>
        }
        <div style={{marginTop: 20}}>
          <Link to="/main">Home</Link>
        </div>
      </div>
    );
  }
});
