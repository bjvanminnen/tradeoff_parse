import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import HamburgerPanel from './HamburgerPanel';
import ActionDropdown from './ActionDropdown';
import { loadFromServer, addHistory, getAllHistoryItems }
  from '../TradeoffActions';
import { activateTradeoffId } from '../reducers/activeTradeoff';
import History from './History';
import ENV from '../utils/environment';
import { Link } from 'react-router';

class Tradeoff extends React.Component {
  static propTypes = {
    isOwner: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    tradeoffSummary: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired
      })
    ).isRequired,
    isWaiting: PropTypes.bool.isRequired,
    failed: PropTypes.bool.isRequired,
    balance: PropTypes.number.isRequired,
    // TODO proper validation here is something along the lines of "only null if
    // isWaiting
    dropdownOptions: PropTypes.array,
  }

  componentWillMount() {
    this.props.dispatch(loadFromServer(this.props.id));
    this.props.dispatch(getAllHistoryItems(this.props.id));
  }

  handleDelta(delta, desc) {
    this.props.dispatch(addHistory({
      delta,
      desc,
      tradeoffId: this.props.id,
      date: new Date()
    }));
  }

  onSwitchTradeoff(newId) {
    this.props.dispatch(activateTradeoffId(newId));
  }

  render() {
    if (this.props.isWaiting) {
      return (<div>Loading...</div>);
    }

    if (this.props.failed) {
      return (<div>Failed</div>);
    }

    const headerText = 'Balance: ' + this.props.name;

    return (
      <span>
        <HamburgerPanel
          readonly={!this.props.isOwner}
          headerText={headerText}
          tradeoffSummary={this.props.tradeoffSummary}
          switchTradeoff={this.onSwitchTradeoff.bind(this)}
        >
          <h4>{this.props.balance}</h4>
        </HamburgerPanel>
        {this.props.isOwner &&
          <ActionDropdown
            dropdownOptions={this.props.dropdownOptions}
            addOperation={this.handleDelta.bind(this)}/>
        }
        <History history={this.props.history}/>
        <div>
          <Link to="/edit">Edit</Link>
        </div>
      </span>
    );
  }
};

export default connect((state) => {
  const activeTradeoff = state.tradeoffs[state.activeTradeoff];
  const tradeoffSummary = Object.keys(state.tradeoffs).map(uid => ({
    name: state.tradeoffs[uid].name,
    id: state.tradeoffs[uid].tradeoffId,
  }));
  return {
    isOwner: activeTradeoff.owner === state.auth.uid,
    name: activeTradeoff.name,
    key: state.activeTradeoff,
    id: state.activeTradeoff,
    tradeoffSummary,
    isWaiting: activeTradeoff.isWaiting,
    failed: activeTradeoff.failed,
    balance: activeTradeoff.balance,
    dropdownOptions: activeTradeoff.dropdownData,
    history: activeTradeoff.history
  };
})(Tradeoff);
