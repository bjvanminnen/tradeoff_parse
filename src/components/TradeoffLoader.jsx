import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Tradeoff from './Tradeoff';
import { loadCurrentUserTradeoff } from '../TradeoffActions';

function tradeoffLoader(TradeoffComponent) {
  class TradeoffLoader extends React.Component {
    componentWillMount() {
      if (!this.props.userName) {
        this.props.router.push('/');
        return;
      }

      if (!this.props.activeTradeoff) {
        this.props.dispatch(loadCurrentUserTradeoff());
      }
    }

    render() {
      if (!this.props.activeTradeoff) {
        return <div>Loading...</div>
      }
      return <TradeoffComponent/>
    }
  }

  return connect(state => ({
    userName: state.auth.userName,
    activeTradeoff: state.activeTradeoff
  }))(withRouter(TradeoffLoader));
};

export default tradeoffLoader;
