import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ButtonLink from './ButtonLink';

const styles = {
  button: {
    margin: 5
  }
}

class HomeScreen extends React.Component {
  componentWillMount() {
    if (this.props.uid) {
      this.props.router.push('/main')
    }
  }

  render() {
    return (
      <div>
        <ButtonLink style={styles.button} to='/signin'>
          Sign In
        </ButtonLink>
        <ButtonLink style={styles.button} to='/signup'>
          Create Account
        </ButtonLink>
      </div>
    );
  }
}

export default connect(state => ({
  uid: state.auth.uid
}))(withRouter(HomeScreen));
