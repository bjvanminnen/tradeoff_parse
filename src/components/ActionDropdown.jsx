import React from 'react';
import InputModal from './InputModal';
import {DropdownButton, MenuItem, ModalTrigger, Modal
  } from 'react-bootstrap';

export default class extends React.Component {
  static propTypes = {
    addOperation: React.PropTypes.func.isRequired,
    dropdownOptions: React.PropTypes.array.isRequired
  }

  constructor(props) {
    super(props);
    this.state = { showModal: false };
  }

  handleSelectItem(item) {
    this.props.addOperation(item.delta, item.desc || item.label);
  }

  handleSelectModal() {
    this.setState({showModal: true });
  }

  handleModalClose() {
    this.setState({showModal: false});
  }

  render() {
    return (
      <span>
        <DropdownButton
            id='actionDropdown'
            bsStyle='primary'
            title='Register Action'>
          {this.renderMenuItems(this.props.dropdownOptions)}
        </DropdownButton>
        <InputModal
            show={this.state.showModal}
            onHide={this.handleModalClose.bind(this)}
            addOperation={this.props.addOperation.bind(this)}
        />
      </span>
    );
  }

  renderMenuItems(json) {
    let key = 0;
    return json.map((group) => {
      if (group.children) {
        return [
          this.renderHeader(key++, group.name),
          ...group.children.map(child => this.renderRow(key++, child)),
          this.renderDivider(key++)
        ];
      } else {
        // We assume custom
        return this.renderModalOpener(key++, group.name);
      }
    })
    .reduce((accumulator, item) => {
      return accumulator.concat(item);
    }, []);
  }

  renderModalOpener(key, label) {
    return <MenuItem
        key={key++}
        onSelect={this.handleSelectModal.bind(this)}>
      {label}
    </MenuItem>;
  }

  renderDivider(key) {
    return <MenuItem key={key} divider={true}/>
  }

  renderHeader(key, name) {
    return <MenuItem key={key} header={true}>
      {name}
    </MenuItem>
  }

  renderRow(key, data) {
    return <MenuItem
        key={key}
        eventKey={data}
        onSelect={this.handleSelectItem.bind(this)}>
      {data.label}
    </MenuItem>
  }
}
