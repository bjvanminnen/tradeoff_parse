import React from 'react';
import {Panel, Glyphicon, DropdownButton, MenuItem, ButtonGroup
  } from 'react-bootstrap';
import ENV from '../utils/environment';

// TODO - a better way to organize this component may reveal itself, as this is
// a little wonky
export default class HamburgerPanel extends React.Component {
  static propTypes = {
    readonly: React.PropTypes.bool.isRequired,
    headerText: React.PropTypes.string.isRequired,
    tradeoffSummary: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.string.isRequired,
      name: React.PropTypes.string.isRequired
    })).isRequired,
    switchTradeoff: React.PropTypes.func.isRequired
  };

  handleHamburgerClick(tradeoffId) {
    this.props.switchTradeoff(tradeoffId);
  }

  render() {
    const rightAligned = {
      float: 'right',
      marginTop: -6
    };

    const header = (
      <span>
        {this.props.headerText}
        <ButtonGroup style={rightAligned}>
          <DropdownButton
              id='hamburgeDropdown'
              bsStyle='primary'
              noCaret={true}
              pullRight={true}
              title={<Glyphicon glyph='menu-hamburger'/>}>
          {
            this.props.tradeoffSummary.map(item => {
              return (
                <MenuItem
                    key={item.id}
                    onSelect={this.handleHamburgerClick.bind(this, item.id)}>
                  {item.name}
                </MenuItem>);
            })
          }
          </DropdownButton>
        </ButtonGroup>
      </span>
    );

    return (
      <Panel
        header={header}
        bsStyle={this.props.readonly ? 'info' : 'primary'}
      >
        {this.props.children}
      </Panel>
    );
  }
}
