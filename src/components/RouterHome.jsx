import React from 'react';
import { Router, Route, hashHistory, browserHistory } from 'react-router';

import HomeScreen from './HomeScreen';
import tradeoffLoader from './TradeoffLoader';
import Tradeoff from './Tradeoff';
import EditTradeoff from './EditTradeoff';
import Signup from './Signup';
import Signin from './Signin';

export default class extends React.Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={HomeScreen}/>
        <Route path="signin" component={Signin}/>
        <Route path="signup" component={Signup}/>
        <Route path="main" component={tradeoffLoader(Tradeoff)}/>
        <Route path="edit" component={tradeoffLoader(EditTradeoff)}/>
      </Router>
    );
  }
}
