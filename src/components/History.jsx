import React from 'react';
import {Table} from 'react-bootstrap';
import PaginatedTable from './PaginatedTable';
import Immutable from 'immutable';
import { connect } from 'react-redux';
import HistoryRow from './HistoryRow';

export default class extends React.Component {
  static propTypes = {
    history: React.PropTypes.instanceOf(Immutable.List).isRequired
  }

  generateRow(index) {
    let item = this.props.history.get(index);
    if (!item) {
      return null;      
    }
    return (
      <HistoryRow
        key={index}
        delta={item.delta}
        date={item.date}
        desc={item.desc}
        state={item.state}/>
    );
  }

  render() {
    let tableStyle = {
      marginTop: 20
    };
    return (
      <PaginatedTable
          style={tableStyle}
          size={this.props.history.size}
          pageSize={10}
          rowGenerator={this.generateRow.bind(this)}>
        <thead>
          <tr>
            <th>Delta</th>
            <th>Description</th>
            <th>Date</th>
          </tr>
        </thead>
      </PaginatedTable>
    );
  }
};
