var React = require('react');
var Button = require('react-bootstrap').Button;
var Modal = require('react-bootstrap').Modal;
var Panel = require('react-bootstrap').Panel;

var InputModal = module.exports = React.createClass({
  propTypes: {
    show: React.PropTypes.bool.isRequired,
    onHide: React.PropTypes.func.isRequired,
    addOperation: React.PropTypes.func.isRequired
  },

  handleClick: function () {
    var delta = parseInt(this.refs.delta.value, 10);
    var desc = this.refs.desc.value;
    this.props.addOperation(delta, desc);
    this.props.onHide();
  },

  render: function () {
    var outerDivStyle = {
      textAlign: 'center',
      marginTop: 20,
      marginBottom: 20
    };

    var inputStyle = {
      marginTop: 10,
      marginBottom: 10
    };

    return (
      <Modal
          onHide={this.props.onHide}
          show={this.props.show}
          bsSize='small'
      >
        <div style={outerDivStyle}>
          <h4>Description</h4>
          <div style={inputStyle}>
            <input ref='desc' style={{width: '80%'}}/>
          </div>

          <h4>Delta</h4>
          <div style={inputStyle}>
            <input ref='delta' type='number' style={{width: '20%'}}/>
          </div>
          <Button
              bsStyle='primary'
              onClick={this.handleClick}>
            Submit
          </Button>
        </div>
      </Modal>
    );
  }
});
