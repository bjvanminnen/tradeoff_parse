import React from 'react';
import { withRouter } from 'react-router';

class ButtonLink extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { router, to } = this.props;
    router.push(to);
  }

  render() {
    const { router, to, ...rest} = this.props;
    return (
      <button onClick={this.onClick} {...rest}>
        {this.props.children}
      </button>
    );
  }
}

export default withRouter(ButtonLink);
