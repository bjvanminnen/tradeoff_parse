import React from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { doAuth, signIn } from '../TradeoffActions';

const styles = {
  col1: {
    textAlign: 'right',
    paddingRight: 5
  },
  input: {
    paddingTop: 2,
    paddingBottom: 2
  }
};

class Signin extends React.Component {
  constructor(props) {
    super(props);

    this.signIn = this.signIn.bind(this);
    this.createAccount = this.createAccount.bind(this);
  }

  componentDidUpdate() {
    if (this.props.uid) {
      this.props.router.push('/main')
    }
  }

  signIn() {
    this.props.signIn(this.refs.email.value, this.refs.password.value);
  }

  createAccount() {
    this.props.router.push('/signup')
  }

  render() {
    return (
      <div>
        <div>
          Note things are very hacked together at the moment. Please don't make
          an effort to break them.
        </div>
        <div>
          <b>Display name</b> is used to differentiate you from other users.
        </div>
        <div>
          <b>Email</b> is used for potential password resets in the future
        </div>
        <h3>Sign In</h3>
        <table>
          <tbody>
            <tr>
              <td style={styles.col1}>Email:</td>
              <td style={styles.input}>
                <input ref='email' type="email"/>
              </td>
            </tr>
            <tr>
              <td style={styles.col1}>Password:</td>
              <td style={styles.input}>
                <input ref='password' type="password"/>
              </td>
            </tr>
            <tr>
              <td colSpan="2" style={{textAlign: 'right'}}>
                <button onClick={this.signIn}>Sign in</button>
              </td>
            </tr>
          </tbody>
        </table>
        {this.props.authFailure &&
          <div style={{color: 'red'}}>
            {this.props.authFailure}
          </div>
        }
      </div>
    )
  }
}

export default connect(state => ({
  uid: state.auth.uid,
  authFailure: state.auth.failureMessage
}), dispatch => ({
  doAuth() {
    dispatch(doAuth());
  },
  signIn(email, password) {
    dispatch(signIn(email, password));
  }
}))(withRouter(Signin));
