var webpack = require('webpack');

var BUILD_DIR = 'static/js/built/';

var config = module.exports = {
  entry: {
    main: [
      __dirname + '/node_modules/babel-core/browser-polyfill.js',
      './src/main.jsx'
    ]
  },
  output: {
    filename: BUILD_DIR + '[name].js'
  },
  module: {
    loaders: [
      {
        test: /tradeoff_parse\/[src|test].*\.jsx*$/,
        loaders: [
          'babel-loader?stage=0'
        ]
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
   ],
    noParse : [
      /\/babel-core\/browser-polyfill\.js$/
    ],
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx"]
  },
  devtool: "inline-source-map",
  plugins: [
    new webpack.optimize.CommonsChunkPlugin(BUILD_DIR + 'common.js'),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'VERSION': JSON.stringify(process.env.VERSION || '')
    })
  ]
}

if (process.env.NODE_ENV === 'production') {
  config.devtool = undefined;
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      sourceMap: false,
      compress: {
        warnings: false
      }
    })
  );
}
