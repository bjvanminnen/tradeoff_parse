/**
 * Create a basic DOM if we don't already have one. Depends on jsdom being loaded
 * into the global namespace for node environment.
 */
(function () {
  let haveDom = !!window.document;
  if (haveDom) {
    return; // we already have a DOM
  }

  if (!global.jsdom) {
    throw new Error('expect o have jsdom if not a browser');
  }

  global.document = global.jsdom.jsdom('<!doctype html><html><body></body></html>');
  global.window = document.parentWindow;
})();
