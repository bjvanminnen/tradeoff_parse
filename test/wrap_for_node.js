// Needed to make un-mocked requests in node
global.XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

global.window = {};
global.jsdom = require('jsdom');
global.firebase = {};
require('./test_output');
