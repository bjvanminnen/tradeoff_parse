import {assert} from 'chai';
import { ACTIVATE_TRADEOFF_ID } from 'reducers/activeTradeoff';
import dropdownEditReducer, {
  UPDATE_DROPDOWN_BEGIN,
  UPDATE_DROPDOWN_SUCCESS,
  UPDATE_DROPDOWN_FAILURE
} from 'reducers/dropdownEdit';

describe('dropdownEditReducer', () => {
  describe('activateTradeoffId', () => {
    const action = {
      type: ACTIVATE_TRADEOFF_ID,
      body: 'id1'
    };

    it('creates basic state on activateTradeoffId', () => {
      const newState = dropdownEditReducer({}, action);
      assert.deepEqual(newState, {
        id1: {
          modifying: false,
          failed: false
        }
      });
    });

    it("doesn't change tradeoff's state when reactivated", () => {
      const startState = {
        id1: {
          modifying: true,
          failed: false
        }
      };
      const newState = dropdownEditReducer(startState, action);
      assert.strictEqual(newState.id1, startState.id1);
      assert.equal(startState.id1.modifying, true);
    });
  });

  describe('updateDropdownData', () => {
    const startState = {
      id1: {
        modifying: true,
        failed: false
      }
    };

    it('sets modifying/failed to true/false on update begin', () => {
      const newState = dropdownEditReducer(startState, {
        type: UPDATE_DROPDOWN_BEGIN,
        body: { tradeoffId: 'id1' }
      });
      assert.deepEqual(newState.id1, {
        modifying: true,
        failed: false
      });
    });

    it('sets modifying/failed to false/false on update success', () => {
      const newState = dropdownEditReducer(startState, {
        type: UPDATE_DROPDOWN_SUCCESS,
        body: { tradeoffId: 'id1' }
      });
      assert.deepEqual(newState.id1, {
        modifying: false,
        failed: false
      });
    });

    it('sets modifying/failed to false/true on update failure', () => {
      const newState = dropdownEditReducer(startState, {
        type: UPDATE_DROPDOWN_FAILURE,
        body: { tradeoffId: 'id1' }
      });
      assert.deepEqual(newState.id1, {
        modifying: false,
        failed: true
      });
    });
  });
});
