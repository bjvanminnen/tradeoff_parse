import {assert} from 'chai';
import Immutable from 'immutable';

import {singleTradeoffReducer} from 'reducers/tradeoffs';
import {
  LOAD_TRADEOFFS,
  LOAD_FROM_SERVER_SUCCESS,
  LOAD_FROM_SERVER_FAILURE
} from 'TradeoffActions';

import { UPDATE_DROPDOWN_SUCCESS } from 'reducers/dropdownEdit';


// TODO - a lot of deepEquals prob make more sense as strictEqual

describe('singleTradeoffReducer', () => {
  const tradeoffId = 'my_tradeoff';

  describe('loadFromServer', () => {
    it('loadFromServer_success', () => {
      const balance = 10;
      const name = 'my_name';
      const dropdownData = {
        someObject: true
      };

      const action = {
        type: LOAD_FROM_SERVER_SUCCESS,
        body: { tradeoffId, balance, name, dropdownData }
      };

      assert.deepEqual(
        singleTradeoffReducer({}, action),
        {
          tradeoffId,
          balance,
          name,
          dropdownData,
          isWaiting: false
        }
      );

      const previousState = {
        balance: 0,
        tradeoffId: null,
        failed: false,
        history: Immutable.List()
      };
      assert.deepEqual(
        singleTradeoffReducer(previousState, action),
        {
          tradeoffId,
          balance,
          name,
          dropdownData,
          isWaiting: false,
          failed: previousState.failed,
          history: previousState.history
        }
      );
    });

    it('loadFromServer_failure', () => {
      const startState = {
        balanace: 0,
        failed: false,
        isWaiting: true
      };

      const action = {
        type: LOAD_FROM_SERVER_FAILURE,
        body: {tradeoffId},
        error: new Error('something')
      };

      const newState = singleTradeoffReducer(startState, action);

      assert.deepEqual(newState.balance, startState.balance);
      assert.deepEqual(newState.failed, true);
      assert.deepEqual(newState.isWaiting, false);
    });
  });

  describe('addHistory', () => {
    const delta = -10;
    const date = new Date();
    const desc = 'description';

    const beginAction = {
      type: 'addHistory_begin',
      body: { tradeoffId, delta, date, desc }
    };

    const delta2 = 20;
    // make sure we have a different time
    const date2 = new Date(date.getTime() + 1);
    const desc2 = 'description2';

    const beginAction2 = {
      type: 'addHistory_begin',
      body: {
        tradeoffId,
        delta: delta2,
        date: date2,
        desc: desc2
      }
    };

    describe('addHistory_begin', function () {
      it('throws if we dont have state for this tradeoff', () => {
        assert.throw(() => {
          singleTradeoffReducer(null, action);
        });
      });

      it('adjusts the balance', () => {
        const newState = singleTradeoffReducer({
          balance: 20,
          history: Immutable.List()
        }, beginAction);
        assert.deepEqual(newState.balance, 10);
      });

      it('appends a new history item to an empty list', () => {
        const history = Immutable.List();
        const newState = singleTradeoffReducer({
          balance: 20,
          history
        }, beginAction);

        const newHistory = newState.history;
        assert.deepEqual(newHistory.size, 1);
        const item = newHistory.get(0);
        assert.deepEqual(item.date, date);
        assert.deepEqual(item.desc, desc);
        assert.deepEqual(item.delta, delta);
        assert.deepEqual(item.state, 'pending');
      });

      it('appends a new history item to a non-empty list', () => {
        const history = Immutable.List();
        const middleState = singleTradeoffReducer({
          balance: 20,
          history
        }, beginAction);
        const newState = singleTradeoffReducer(middleState, beginAction2);

        assert.deepEqual(middleState.history.size, 1);
        assert.strictEqual(newState.history.get(1), middleState.history.get(0));

        const newHistory = newState.history;
        assert.deepEqual(newHistory.size, 2);

        const item = newHistory.get(0);
        assert.deepEqual(item.date, date2);
        assert.deepEqual(item.desc, desc2);
        assert.deepEqual(item.delta, delta2);
        assert.deepEqual(item.state, 'pending');
      });
    });

    describe('addHistory_success', () => {
      it('sets the correct item to completed', () => {
        let states = {};
        states.first = singleTradeoffReducer({
          balance: 20,
          history: Immutable.List()
        }, beginAction);
        states.second = singleTradeoffReducer(states.first, beginAction2);

        const completionAction = {
          type: 'addHistory_success',
          body: { date }
        };

        const completionAction2 = {
          type: 'addHistory_success',
          body: { date: date2 }
        };

        let newState = singleTradeoffReducer(states.second, completionAction);
        assert.deepEqual(newState.history.size, 2);
        // first date is now at the end of the list, so index 1
        const item = newState.history.get(1);
        assert.deepEqual(item.state, 'completed');

        // other item didn't change
        assert.strictEqual(newState.history.get(0), newState.history.get(0));

        // same thing happens if we complete the other item instead
        newState = singleTradeoffReducer(states.second, completionAction2);
        assert.deepEqual(newState.history.size, 2);
        assert.deepEqual(newState.history.get(0).state, 'completed');
        assert.deepEqual(newState.history.get(1).state, 'pending');
      });
    });

    it('addHistory_failure', () => {
      assert.throw(() => {
        const failureAction = {
          type: 'addHistory_failure',
          body: {tradeoffId},
          error: new Error('failed')
        };
        singleTradeoffReducer({
          balance: 20,
          history: Immutable.List()
        }, failureAction);
      }, /failed/);
    });
  });

  describe('updateDropdownData', () => {
    it('modifies the existing dropdown', () => {
      const startState = {
        balance: 0,
        tradeoffId: null,
        failed: false,
        dropdownData: {
          foo: 'bar'
        },
        history: Immutable.List(),
      };
      const action = {
        type: UPDATE_DROPDOWN_SUCCESS,
        body: {
          newDropdownData: {
            newData: 'here'
          }
        }
      };
      const newState = singleTradeoffReducer(startState, action);
      assert.deepEqual(newState.dropdownData, { newData: 'here' });
    });
  });

});
