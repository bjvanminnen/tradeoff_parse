import {assert} from 'chai';
import Immutable from 'immutable';

import tradeoffsReducer from 'reducers/tradeoffs';
import { loadTradeoffSummary, LOAD_FROM_SERVER_SUCCESS } from 'TradeoffActions';

describe('tradeoffsReducer', () => {
  describe('loadTradeoffSummary', () => {
    const action = loadTradeoffSummary({
      uid1: {
        name: 'name1',
        id: 'id1'
      },
      uid2: {
        name: 'name2',
        id: 'id2'
      }
    });

    const startState = {
      id1: {
        balance: 20,
        tradeoffId: 'id1',
        name: 'foo',
        isWaiting: false,
        failed: false
      }
    };
    let newState;

    before(() => {
      newState = tradeoffsReducer(startState, action);
    });

    it('initializes for new tradeoffIds', () => {
      assert.strictEqual(Object.keys(newState).length, 2);
      assert.strictEqual(newState.id2.balance, 0);
      assert.strictEqual(newState.id2.tradeoffId, 'id2');
      assert.strictEqual(newState.id2.name, 'name2');
      assert.strictEqual(newState.id2.isWaiting, true);
      assert.strictEqual(newState.id2.failed, false);
      assert.strictEqual(newState.id2.history, Immutable.List());
    });

    it ('does not overwrite state for existing tradeoffIds', () => {
      assert.strictEqual(newState.id1, startState.id1);
    });
  });

  describe('id-specific actions', () => {
    const startState = {
      id1: 'foo',
      id2: 'bar',
      id3: {}
    };

    it('modifies a single child for id-specific actions', () => {
      const action = {
        type: LOAD_FROM_SERVER_SUCCESS,
        body: { tradeoffId: 'id3' }
      };

      const newState = tradeoffsReducer(startState, action);
      assert.strictEqual(startState.id1, newState.id1);
      assert.strictEqual(startState.id2, newState.id2);
      assert.notStrictEqual(startState.id3, newState.id3);
    });

    it('does nothing if action doesnt have an id', () => {
      const action = {
        type: 'loadFromServer_success',
        body: { }
      };
      const newState = tradeoffsReducer(startState, action);
      assert.strictEqual(startState, newState);
    });
  });
});
