import {assert} from 'chai';

import activeTradeoffReducer,
  { ACTIVATE_TRADEOFF_ID } from 'reducers/activeTradeoff';

describe('activeTradeoffReducer', () => {
  const activateAction1 = {
    type: ACTIVATE_TRADEOFF_ID,
    body: 'tradeoff1'
  };

  const activateAction2 = {
    type: ACTIVATE_TRADEOFF_ID,
    body: 'tradeoff2'
  };

  it('set activeTradeoff when activated', () => {
    assert.deepEqual(activeTradeoffReducer(null, activateAction1), 'tradeoff1');
    assert.deepEqual(activeTradeoffReducer('tradeoff1', activateAction2), 'tradeoff2');
  });
});
