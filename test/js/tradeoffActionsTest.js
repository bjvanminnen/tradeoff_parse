import { assert } from 'chai';

/**
 * Helper for testing async actions. Creates a dispatcher that collects all
 * actions, then when finishCondition is true, calls validator.
 */
// function createDispatchCollector(finishCondition, validator) {
//   let collection = [];
//   return action => {
//     collection.push(action);
//     if (finishCondition(collection)) {
//       setTimeout(() => validator(collection), 0);;
//     }
//   };
// }

// TODO - mock and rewrite tests for firebase backend?

// describe('tradeoffActions', () => {
//   beforeEach(() => {
//     environment.clear();
//   });
//
//   describe('loadTradeoffs', () => {
//     it('has both primary and secondary key if set', () => {
//       environment.set({
//         TRADEOFF_ID: 'tid1',
//         SECONDARY_TRADEOFF_ID: 'tid2'
//       });
//
//       const action = loadTradeoffs();
//       assert.deepEqual(action, {
//         type: 'loadTradeoffs',
//         body: ['tid1', 'tid2']
//       });
//     });
//
//     it('has only primary key if no second key in env', () => {
//       environment.set({
//         TRADEOFF_ID: 'tid1'
//       });
//
//       const action = loadTradeoffs();
//       assert.deepEqual(action, {
//         type: 'loadTradeoffs',
//         body: ['tid1']
//       });
//     });
//   });
//
//   it('pageLoad', () => {
//     environment.set({
//       TRADEOFF_ID: 'tid1'
//     });
//
//     const action = pageLoad();
//     assert.deepEqual(action, activateTradeoffId('tid1'));
//   });
//
//   it('activateTradeoffId', () => {
//     const action = activateTradeoffId('tid');
//     assert.deepEqual(action, {
//       type: 'activateTradeoffId',
//       body: 'tid'
//     });
//   });
//
//   it('deactivateTradeof', () => {
//     const action = deactivateTradeoff();
//     assert.deepEqual(action, {
//       type: 'activateTradeoffId'
//     });
//   });
//
//   describe('async actions', () => {
//     afterEach(() => {
//       ParseAccessFaker.restore();
//     });
//
//     it('loadFromServer - success', done => {
//       const balance = 50;
//       const dropdownData = { ImADropdown: true };
//
//       ParseAccessFaker.mockAsync({
//         methodName: 'getTradeoff',
//         success: true,
//         result: {
//           tradeoffId: 'tid',
//           balance,
//           dropdownData
//         }
//       });
//
//       const asyncAction = loadFromServer('tid');
//       assert.equal(typeof(asyncAction), 'function');
//
//       const dispatcher = createDispatchCollector(actions => actions.length === 2, validate);
//       asyncAction(dispatcher);
//
//       function validate(actions) {
//         assert.deepEqual(actions[0], {
//           type: 'loadFromServer_begin',
//           body: {
//             tradeoffId: 'tid'
//           }
//         });
//         assert.deepEqual(actions[1], {
//           type: 'loadFromServer_success',
//           body: {
//             tradeoffId: 'tid',
//             balance,
//             dropdownData
//           }
//         });
//
//         done();
//       }
//     });
//
//     it('loadFromServer - failure', done => {
//       const err = new Error('unauthorized');
//       ParseAccessFaker.mockAsync({
//         methodName: 'getTradeoff',
//         success: false,
//         result: err
//       });
//
//       const asyncAction = loadFromServer('tid');
//       assert.equal(typeof(asyncAction), 'function');
//
//       const dispatcher = createDispatchCollector(actions => actions.length === 2, validate);
//       asyncAction(dispatcher);
//
//       function validate(actions) {
//         assert.deepEqual(actions[0], {
//           type: 'loadFromServer_begin',
//           body: {
//             tradeoffId: 'tid'
//           }
//         });
//         const failureAction = actions[1];
//         assert.deepEqual(actions[1], {
//           type: 'loadFromServer_failure',
//           body: {
//             tradeoffId: 'tid',
//           },
//           error: err
//         });
//         done();
//       }
//     });
//
//     it('addHistory - success', done => {
//       const tradeoffId = 'tid';
//       const date = new Date();
//
//       ParseAccessFaker.mockAsync({
//         methodName: 'addHistoryItem',
//         success: true,
//         result: {
//           tradeoffId,
//           date: date.toISOString()
//         }
//       });
//
//       const asyncAction = addHistory({
//         tradeoffId: tradeoffId,
//         delta: 10,
//         desc: 'my_desc',
//         date: date
//       });
//       assert.equal(typeof(asyncAction), 'function');
//
//       const dispatcher = createDispatchCollector(actions => actions.length === 2, validate);
//       asyncAction(dispatcher);
//
//       function validate(actions) {
//         assert.deepEqual(actions[0], {
//           type: 'addHistory_begin',
//           body: {
//             tradeoffId,
//             delta: 10,
//             desc: 'my_desc',
//             date
//           }
//         });
//         assert.deepEqual(actions[1], {
//           type: 'addHistory_success',
//           body: {
//             tradeoffId,
//             date: date.toISOString()
//           }
//         });
//         done();
//       }
//     });
//
//     it('addHistory - failure', done => {
//       const tradeoffId = 'tid';
//       const date = new Date();
//       const err = Error('addHistoryFailure');
//
//       ParseAccessFaker.mockAsync({
//         methodName: 'addHistoryItem',
//         success: false,
//         result: err
//       });
//
//       const asyncAction = addHistory({
//         tradeoffId: tradeoffId,
//         delta: 10,
//         desc: 'my_desc',
//         date: date
//       });
//       assert.equal(typeof(asyncAction), 'function');
//
//       const dispatcher = createDispatchCollector(actions => actions.length === 2, validate);
//       asyncAction(dispatcher);
//
//       function validate(actions) {
//         assert.deepEqual(actions[1], {
//           type: 'addHistory_failure',
//           body: {
//             tradeoffId
//           },
//           error: err
//         });
//         done();
//       }
//     });
//
//     it('updateDropdownData - success', done => {
//       const tradeoffId = 'tid';
//       const newDropdown = { data: 'New Data' };
//
//       ParseAccessFaker.mockAsync({
//         methodName: 'uploadDropdownData',
//         success: true,
//         result: undefined
//       });
//
//       const asyncAction = updateDropdownData(tradeoffId, newDropdown);
//       assert.equal(typeof(asyncAction), 'function');
//
//       const dispatcher = createDispatchCollector(actions => actions.length === 2, validate);
//       asyncAction(dispatcher);
//
//       function validate(actions) {
//         assert.deepEqual(actions[0], {
//           type: UPDATE_DROPDOWN_BEGIN,
//           body: {
//             tradeoffId
//           }
//         });
//         assert.deepEqual(actions[1], {
//           type: UPDATE_DROPDOWN_SUCCESS,
//           body: {
//             tradeoffId,
//             newDropdownData: newDropdown
//           }
//         });
//         done();
//       }
//     });
//
//     it('updateDropdownData - failure', done => {
//       const tradeoffId = 'tid';
//       const newDropdown = { data: 'New Data' };
//       const err = new Error('my_err');
//
//       ParseAccessFaker.mockAsync({
//         methodName: 'uploadDropdownData',
//         success: false,
//         result: err
//       });
//
//       const asyncAction = updateDropdownData(tradeoffId, newDropdown);
//       assert.equal(typeof(asyncAction), 'function');
//
//       const dispatcher = createDispatchCollector(actions => actions.length === 2, validate);
//       asyncAction(dispatcher);
//
//       function validate(actions) {
//         assert.deepEqual(actions[1], {
//           type: UPDATE_DROPDOWN_FAILURE,
//           body: { tradeoffId },
//           error: err
//         });
//         done();
//       }
//     });
//   });
// });
