import {assert} from 'chai';
import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import '../../utils/dom';
import HistoryRow from 'components/HistoryRow';

describe('HistoryRow', () => {
  function renderInsideTable(component) {
    return TestUtils.renderIntoDocument(
      <table><tbody>{component}</tbody></table>
    );
  }

  it('has tds for delta/date/desc', () => {
    const date = new Date('09/13/1984');
    let component = renderInsideTable(
      <HistoryRow
        delta={10}
        date={date}
        desc="my_desc"
        state="pending"/>
    );

    const element = ReactDOM.findDOMNode(component);
    const rows = element.querySelectorAll('tr');
    assert.equal(rows.length, 1);

    const row = rows[0];
    assert.equal(row.children.length, 3);
    assert.equal(row.children[0].textContent, "10");
    assert.equal(row.children[1].textContent, "my_desc");
    assert.equal(row.children[2].textContent, "09/13 12:00am");
  });

  it('has opacity 0.5 while pending', () => {
    let component = renderInsideTable(
      <HistoryRow
        delta={10}
        date={new Date()}
        desc="my_desc"
        state="pending"/>
    );

    const element = ReactDOM.findDOMNode(component);
    const rows = element.querySelectorAll('tr');
    assert.equal(rows[0].style.opacity, 0.5);
  });

  it('has no opacity set when', () => {
    let component = renderInsideTable(
      <HistoryRow
        delta={10}
        date={new Date()}
        desc="my_desc"
        state="completed"/>
    );

    const element = ReactDOM.findDOMNode(component);
    const rows = element.querySelectorAll('tr');
    assert.equal(rows[0].style.opacity, '');
  });
});
