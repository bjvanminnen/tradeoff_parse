var webpack = require('webpack');
var glob = require('glob');
var spawn = require('child_process').spawn;

function SuccessPlugin(command) {
  this.command = command;
}

SuccessPlugin.prototype.apply = function (compiler) {
  if (process.argv.indexOf('--mocha') === -1) {
    return;
  }

  var command = this.command;
  compiler.plugin('done', function () {
    spawn('/bin/sh', ['-c', command], { customFds: [0,1,2] })
  });
}

var config = module.exports = {
  entry: glob.sync(__dirname + '/js/**/*.js').concat(
    __dirname + '/../node_modules/babel-core/browser-polyfill.js'
  ),
  output: {
    filename: __dirname + '/test_output.js'
  },
  externals: {
    firebase: "var firebase"
  },
  module: {
    loaders: [
     {
        // test: /js\/.*\.jsx*$/,
        test: /tradeoff_parse\/[src|test].*\.jsx*$/,
        loaders: [
          'babel-loader?stage=0'
        ]
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
   ],
    noParse : [
      /\/babel-core\/browser-polyfill\.js$/
    ],
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx"],
    fallback: [
      __dirname + '/../src'
    ]
  },
  plugins: [
    new SuccessPlugin('mocha test/wrap_for_node.js')
  ],
  devtool: "#inline-source-map",
};
